# Projects

While you can look through my repository at the many projects I've produced. These are a few of the one I personally proud of.

## [IdeaDog](https://github.com/Ostoyae/ideaDog_server) --Server--
---
![Rust] ![Actix] ![ArangoDB]

IdeaDog is a social web application for sharing ideas.
Front-end of the application is built in React, with TypeScript, while the back-end runs on Rust.

My task was to create the back-end while my partner [Brennan D Baraban](https://github.com/bdbaraban/) handled the front-end.

## [AirBnB Clone](https://github.com/Ostoyae/HBNB_Airbnb)
---
![Python] ![Flask] ![Mysql]

This project is a Simple clone of AirBnB and is also the accumulative produced of what I've learn while a studying at the Holberton School.

## [Simple Shell](https://github.com/suhearsawho/simple_shell)
---
![C]

This was my second collaborative project wile attending Holberton, and what could be consered our final for the low-level track. We had to create a simple shell in the C language.

## [Printf](https://github.com/sazad44/printf)
---

![C]

This was my first collaborative project while attending Holberton, We had reproduce the printf function from the stdio lib for the `C` language.

## [Spadefish](https://github.com/BennettDixon/spadefish_gitapp)
---

![Python] ![Flask] ![MySQL]

This was my second hackathon project ran by my school. Spadefish is a platform for Github users to get a visual graph of their most used coding languages.


<!--  Languages badges -->
[Rust]: https://img.shields.io/badge/Lang-Rust-green.svg?link=http://rust-lang.org
[Python]: https://img.shields.io/badge/Lang-Python-green.svg
[C]: https://img.shields.io/badge/Lang-C-green.svg

<!--  Framework badges -->
[Actix]: https://img.shields.io/badge/Framework-Actix-orange.svg
[Flask]: https://img.shields.io/badge/Framework-Flask-orange.svg

<!-- Database badges -->
[MySQL]: https://img.shields.io/badge/Database-MySQL-blue.svg
[ArangoDB]: https://img.shields.io/badge/Database-ArangoDB-blue.svg

