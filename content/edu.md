# <center>Education</center>

## [Holberton School of Software Engineering](https://www.holbertonschool.com/about)
---
**Attend:** 2018 - 2019

Completed the 9 month foundations course and currently participating in the [career track](https://www.holbertonschool.com/pathway_career_track), looking for a stepping stone to my dream job.


## [Gnomon School of Visual Effects](https://www.gnomon.edu)
---
**Attend:** 2010 - 2012

Completed a 2 year course to become a 3D generalist with a focus in 3D modeling and texturing.




