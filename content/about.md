## <center>About me</center>
<div style="text-align: justify;">My name is Martin Smith, and I am a software engineer that has completed a nine-month coding foundations course at the Holberton School of Software Engineer; I've developed skills across the technical stack: front-end, back-end, database, DevOps, and low level. Before that, I was a 3D generalist with experience in using Maya, Houdini and other creative application to create digital media content, for games and film projects.</div>

### <center>Interests</center>
    
<center>**Biking · Hiking · Tech · Rust**</center>hugo