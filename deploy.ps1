echo "Deploying updates to GitHub..."

# Build the project.
hugo -t hugo-coder # if using a theme, replace with `hugo -t <YOURTHEME>`

# Go To Public folder
cd public
# Add changes to git.
git add .

# Commit changes.
$date=date
$msg="rebuilding site $date"

if ( ($args.Count -eq 1)  ) {
    $msg="$args[1]"
}

git commit -m "$msg"

# Push source and build repos.
git push origin master

# Come Back up to the Project Root
cd ..
